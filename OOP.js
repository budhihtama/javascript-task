class Vehicle {
    speed = 0;
    direction = null;

    goForward(newSpeed) {
        this.direction = 'Maju';
        this.speed = newSpeed;
    };

    stop() {
        this.speed = 0;
        this.direction = null;
    };
};

class Car extends Vehicle {
    color = 'Red';
    wheel = 4;
    speed = '250 Km/h';

    goBack(newSpeed) {
        this.direction = 'Mundur';
        this.speed = newSpeed;
    };

    stop() {
        super.stop();
        this.color = 'Blue';
    };
};

class Motor extends Vehicle {
    color = 'Blue';
    wheel = 2;
    speed = '125 Km/h';

    goBack(newSpeed) {
        this.direction = 'Mundur';
        this.speed = newSpeed;
    };

    stop() {
        super.stop();
        this.color = 'Red';
    };
};

let mobil = new Car;
let motor = new Motor;

console.log(mobil);
console.log(motor);
