//declaration

function luasLingkaranDec (r, d) {
return 3.14 * r**2 
}

function kelilingLingkaranDec (r, d) {
    return 3.14 * d
    }

console.log('Luas Lingkaran:', luasLingkaranDec(7, 14))
console.log('Keliling Lingkaran:', kelilingLingkaranDec(7, 14))

// expression
const luasLingkaranExp = function(r, d) {
    return 3.14 * r**2
}
const kelilingLingkaranExp = function(r, d) {
    return 3.14 * d
}

console.log('Luas Lingkaran:', luasLingkaranExp(7, 14))
console.log('Keliling Lingkaran:', kelilingLingkaranExp(7, 14))

// arrow function
const luasLingkaranArr = (r, d) => 3.14 * r**2
const kelilingLingkaranArr = (r, d) => 3.14 * d

console.log('Luas Lingkaran:', luasLingkaranArr(7, 14))
console.log('Keliling Lingkaran:', kelilingLingkaranArr(7, 14))